import React, {useState} from 'react'
const Counter = () =>{
    const [count , setCount]= useState(0);
    const plus = () =>{
        setCount(count+1)
    }
    const sub = () =>{
        setCount(count-1)
    }
    const reset = () =>{
        setCount(0)
    }
    return(
        <div>
            <h1>Count : {count}</h1>
            <button onClick={plus}>Add</button>
            <button onClick={sub}>Minus</button>
            <button onClick={reset}>Reset</button>
        </div>
    )
}
export default Counter