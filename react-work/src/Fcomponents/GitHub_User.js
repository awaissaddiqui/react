import React, {useState ,useEffect} from 'react'
import axios from 'axios';
const GitHub_User = () => {
    const [list, setList] = useState([]);

    useEffect(() =>{
        const loadData = async() =>{
            const {data} = await axios.get("https://api.github.com/users")
            // console.log(data)
            setList(data)
        }
        loadData();

    },[list])
    return (
    <div>
        <ul>
            {
                list.map(l =>{
                    return <li key={l.login}>{l.login+l.html_url} - <img src={l.avatar_url}/></li>
                })
            }
        </ul>
    </div>
  )
}

export default GitHub_User